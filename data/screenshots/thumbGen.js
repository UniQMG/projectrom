var fs=require("fs");
var Jimp=require("jimp");

var screenshots=JSON.parse(fs.readFileSync("screenshots.json","utf8"));

screenshots.forEach(screenshot=>{
  console.log("parsing "+screenshot.name+"...");
  if(screenshot.thumb!==screenshot.image){
    console.log("skipping "+screenshot.name+" -- thumb already set");
    return;
  }
  Jimp.read("./"+screenshot.image,(err,img)=>{
    console.log("loaded "+screenshot.name);
    img.resize(450,Jimp.AUTO).write("./thumbs/"+screenshot.image,()=>{
      console.log("wrote "+screenshot.name+" to ./thumbs/"+screenshot.image);
      screenshots[screenshots.indexOf(screenshot)].thumb="thumbs/"+screenshot.image;
      fs.writeFileSync("screenshots.json",JSON.stringify(screenshots));
      console.log("set "+screenshot.name+" thumb in screenshots.json");
    });
  });
});
