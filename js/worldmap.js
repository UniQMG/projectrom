const app = new Vue({
  el: '#app',
  data: {
    map: null,
    images: [],
    regions: [],
    activeRegion: null,
    mouse: [0, 0],
    regionMouse: [0, 0]
  },
  computed: {
    imagesAndStyles: function() {
      return this.images.map(image => {
        let { x:ix, y:iy } = image;

        let mx = this.mouse[0];
        let my = this.mouse[1];
        let distance = Math.sqrt((mx - ix) ** 2 + (my - iy) ** 2);
        let maxDistance = 50;
        let maxSize = 15;
        let size = distance < maxDistance
          ? maxSize - distance/(maxDistance/maxSize)
          : 0;
        if (image.hover) size = maxSize;

        let [x,y] = this.canvasCoordinate(ix,iy,false,true);
        return {
          image: image,
          classes: {
            floatingimage: true,
            active: image.hover,
            upperHalf: iy < this.$refs.worldmap.height/2
          },
          style: {
            left: x + 'px',
            top: y + 'px',
            width: size + 'px',
            height: size + 'px',
            transform:
              `translateX(${-size/2 + maxSize/2}px)` +
              `translateY(${-size/2 + maxSize/2}px)`,
          }
        };
      })
    }
  },
  methods: {
    canvasCoordinate: function(x, y, clientCoordinate=true, reverse=false) {
      let imgWidth = this.$refs.worldmap.width;
      let imgHeight = this.$refs.worldmap.height;
      let {
        width: elemWidth,
        height: elemHeight,
        left,
        top
      } = this.$refs.worldmap.getBoundingClientRect();
      if (!clientCoordinate) {
        left = 0;
        top = 0;
      }
      if (reverse) {
        let rx = Math.round((x + left)/imgWidth * elemWidth);
        let ry = Math.round((y + top)/imgHeight * elemHeight);
        return [rx,ry];
      } else {
        let rx = Math.round((x - left)/elemWidth * imgWidth);
        let ry = Math.round((y - top)/elemHeight * imgHeight);
        return [rx,ry];
      }
    },
    worldMapMouseMove: function(evt) {
      this.mouse = this.canvasCoordinate(evt.clientX, evt.clientY);
      let [x,y] = this.mouse;

      if (!this.map) return;
      for (let region of this.regions) {
        let ctx = region.maskContext;
        let [r,g,b,a] = ctx.getImageData(x, y, 1, 1).data;
        if (r == 255) {
          if (this.activeRegion == region) return;
          this.activeRegion = region;
          this.regionMouse[0] = x;
          this.regionMouse[1] = y;
          return;
        }
      }
      this.activeRegion = null;
    },
    mouseLog: function(evt) {
      console.log(this.canvasCoordinate(evt.clientX, evt.clientY));
    },
    mouseLeave: function() {
      this.activeRegion = null;
    },
    redraw: function() {
      if (!this.map) return;
      let canvas = this.$refs.worldmap;
      let ctx = canvas.getContext('2d');
      canvas.width = this.map.naturalWidth;
      canvas.height = this.map.naturalHeight;
      ctx.drawImage(this.map, 0, 0);

      if (this.activeRegion) {
        let region = this.activeRegion;
        ctx.globalCompositeOperation = 'screen';
        ctx.drawImage(region.maskImage, 0, 0);

        ctx.globalCompositeOperation = 'source-over';
        ctx.fillStyle = 'black';
        ctx.strokeStyle = 'black';

        let [x,y] = this.regionMouse;
        ctx.fillRect(x-2, y-2, 4, 4);

        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x+20, y-20);
        ctx.lineTo(x+20, y-30);
        ctx.stroke();

        let text = region.name;
        ctx.font = '14px monospace';

        let width = ctx.measureText(text).width;
        ctx.fillStyle = 'white';
        ctx.strokeStyle = 'black';
        ctx.translate(x+20, y-30);
        ctx.fillRect(-width/2-2, -14, width+4, 14);
        ctx.strokeRect(-width/2-2, -14, width+4, 14);
        ctx.fillStyle = 'black';
        ctx.fillText(text, -width/2, -2)

      }
    }
  },
  watch: {
    map: function() { this.redraw() },
    activeRegion: function() { this.redraw() }
  }
});

setInterval(app.redraw, 16);
(async function() {
  let map = new Image();
  map.src = "data/worldmap/map.png";
  await new Promise(res => map.onload = res);
  app.map = map;

  let regionData = await (await fetch("data/worldmap/regions.json")).json();
  for (let region of regionData) {
    let mask = new Image();
    region.maskImage = mask;
    mask.src = 'data/worldmap/regions/' + region.mask;

    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext('2d');
    region.maskCanvas = canvas;
    region.maskContext = ctx;

    await new Promise(res => {
      mask.onerror = ((err) => {
        console.error("Failed to load mask", err);
        res();
      });
      mask.onload = (() => {
        canvas.width = mask.width;
        canvas.height = mask.height;
        ctx.drawImage(mask, 0, 0);
        res();
      });
    })
  }
  app.regions = regionData;
  app.activeRegion = app.regions[3];

  let screenshotData = await (await fetch("data/screenshots/screenshots.json")).json();
  for (let image of screenshotData) {
    image.hover = false;
  }
  app.images = screenshotData;
})();
